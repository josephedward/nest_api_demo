import { Test } from '@nestjs/testing';
import { CatsController } from './cats.controller';
import { CatsService } from './cats.service';
import { Cat } from './interfaces/cat.interface';

describe('CatsController', () => {
  let catsController: CatsController;
  let catsService: CatsService;

  beforeEach(async () => {
    const moduleRef = await Test.createTestingModule({
      controllers: [CatsController],
      providers: [CatsService],
    }).compile();

    catsService = moduleRef.get<CatsService>(CatsService);
    catsController = moduleRef.get<CatsController>(CatsController);
  });

  describe('findAll', () => {
    it('should return an array of cats', async () => {
      const result: Cat[] = [
        {
          age: 2,
          breed: 'Bombay',
          name: 'Pixel',
          fiesty: true,
        },
      ];
      jest.spyOn(catsService, 'findAll').mockImplementation(() => result);

      expect(await catsController.findAll()).toBe(result);
    });
  });

  describe('postCat', () => {
    it('should successfully create a cat', async () => {
        const result: Cat[] = [
          {
            name: 'testCat',
            age: 5,
            breed: 'cool',
            fiesty: false,
          },
          {
            name: 'George',
            age: 3,
            breed: 'siberian',
            fiesty: true,
          },
        ]
      

      await catsController
        .create({
          name: 'George',
          age: 3,
          breed: 'siberian',
          fiesty: true,
        })
        .then(async () => expect(await catsController.findAll()).toStrictEqual(result));
    });
  });
});
