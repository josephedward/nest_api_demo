import { IsInt, IsString, IsBoolean } from 'class-validator';

export class CreateCatDto {
  @IsString()
  readonly name: string;

  @IsInt()
  readonly age: number;

  @IsString()
  readonly breed: string;

  @IsBoolean()
  readonly fiesty: boolean;
}